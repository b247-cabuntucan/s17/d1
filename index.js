//Javascript Basic Functions
console.log('Hello World!');

// [SECTION] Functions
    // Functions in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked.

// [SECTION] Funcion Declaration
    // This is the part where we declare
    // This is the part where we defined a function with the specified parameters.

    /*
        Syntax:
            function functionName() {
                code block (statement)
            } 
    */

    // Function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

    function printName() {
        console.log("My name is Clark.")
    }

// [SECTION] Function Invocation
    // This is the part where we invoke/call
    // The code block and statements inside a function is not immediately executed when the function is defined.

    printName();

// [SECTION] Function Declaration vs Expressions

    //[SUB-SECTION] Function Declaration

        //Hoisting is Javascript's behavior for certain variable and functions to run or use them before their declaration.


    // variableFunction(); will render an error since thsi si a function expression.

    //[SUB-SECTION] Function Expression
        //A function can also be stored in a variable. This is called a function expression.

            let variableFunction = "red";
            console.log(variableFunction);

        variableFunction = function () {
            console.log("Hello Again! This is a function expression")
        };

        variableFunction();

// [SECTION] Function Scoping
        // Scope is the accessibility (visibility) of variables within our program.
            // 3 Types of Scope
                // 1. local/block scope
                // 2. global scope
                // 3. function scope

    {
        let localVar = 'Wakanda';
        console.log(localVar);
    }

    let globalVar = 'Mr. Worldwide';
    console.log(globalVar);

    let localVar = 'Not Wakanda';
    console.log(localVar); // localVar, being a block, cannot be accessed outside of its code block.

    //Function Scope
        //Javascript has a function scope: Each functions created a new scope.


    function showNames () {
        var functionVar = 'Joe';
        let functionLet = 'Jane';
        const functionConst = 'John';

        console.log(functionVar);
        console.log(functionLet);
        console.log(functionConst);

    }

    showNames();

// [SECTION] Nested Functions

    // You can create another function inside a function.

    function myNewFunction() {
        let name = 'Mike';

        function nestedFunction() {
            let nestedName = 'Scootie';
            console.log(name);
        }

        nestedFunction();
    }

    myNewFunction();

    //nestedFunction(); //this will result to an error

//[SECTION] Function and Global Scoped Variables

    //Global Scoped Variable
    let globalName = 'Alexandro';

    function myNewFunction2() {
        let nameInside = 'Renz';

        console.log(globalName);
    }

    myNewFunction2();

//[Section] using alert()
    alert('Mabuhay!\nThis is allert')

    console.log('I will only log in the console when the alert is dismissed')

    /*
    Notes on the use of alert():
        - Show onl an alert() for short dilogs/message to the user
        -Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.
    */

// [SECTION] Using prompt()

    let samplePrompt = prompt('Enter your name.')
    console.log(typeof samplePrompt);
    console.log(samplePrompt);
    console.log('Hello, ' + samplePrompt + "!");

        alert('Hello again, ' + samplePrompt + "!");

        /*
            prompt() Syntax:

            promt('dialogInString);
        */
    
    function printWelcomeMessage(){
        let firstName = prompt('Kindly Enter Your First Name')
        let lastName = prompt('Kindly Enter Your Last Name')

        console.log('Hello, ' + firstName + " " + lastName + "!");
        alert('Welcome to my page.');
    }

    printWelcomeMessage();

//[SECTION] Function Naming Conventions

    //Functions names should be definitive of the task it will perform.
    //Make it relevant.
    //It usually contains a verb.
    //

    function getCourses(){
        let courses = ['Science 101', 'Math 101', 'English 101'];
        console.log(courses);
    }

    getCourses()
